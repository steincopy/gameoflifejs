function Cell(row, col) {
    this.row = row;
    this.col = col;
}

Cell.prototype.nextGen = function (universe) {
    var count = 0;
    for (var i = this.row - 1; i < this.row + 2; i++) {
        for (var j = this.col - 1; j < this.col + 2; j++) {
            if ( (i == this.row && j == this.col) || universe[i] === undefined || universe[i][j] === undefined) {
                continue;
            } else if (universe[i][j].alive) {
                count++;
            }
        }
    }
    if ( (this.alive == true && (count == 2 || count == 3)) || (this.alive == false && count == 3) ) {
        return true;
    } else {
        return false;
    }
};

Cell.prototype.alive = false;

var intervID;

function getElements() {

    var elements = [
        document.getElementsByClassName("universe")[0],
        document.getElementsByTagName("tr"),
        document.getElementsByClassName("cell"),
        document.getElementsByClassName("refresh")[0],
        document.getElementsByClassName("start")[0],
        document.getElementsByClassName("hold")[0],
        document.getElementsByClassName("generation")[0],
        document.getElementsByClassName("random")[0]
    ]

    var generation = 1;

    return {
        universe: function() {
            return elements[0];
        },
        trs: function() {
            return elements[1];
        },
        tds: function() {
            return elements[2];
        },
        refresh: function() {
            return elements[3];
        },
        startGame: function() {
            return elements[4];
        },
        holdGame: function() {
            return elements[5];
        },
        nxtgen: function() {
            generation++;
            elements[6].innerHTML = generation;
        },
        resetgen: function() {
            generation = 1;
            elements[6].innerHTML = generation;
        },
        randomButton: function() {
            return elements[7];
        }
    };

}

function init(elems) {

    for (var i = 0; i < 30; i++) {
        var tr = elems.universe().insertRow(i);
        for (var j = 0; j < 30; j++) {
            tr.innerHTML += "<td class=\"cell " + i + "_" + j + " dead\"></td>";
        }
    }

    for (var i = 0; i < elems.tds().length; i++) {
        elems.tds()[i].onclick = function(e) {
            if (e.target.classList.contains("dead")) {
                e.target.classList.remove("dead");
                e.target.classList.add("alive");
            } else {
                e.target.classList.remove("alive");
                e.target.classList.add("dead");
            }
        };
    }

    elems.refresh().onclick = function() {
        clearInterval(intervID);
        elems.resetgen();
        for (var i = 0; i < elems.tds().length; i++) {
            if (elems.tds()[i].classList.contains("alive")) {
                elems.tds()[i].classList.remove("alive");
                elems.tds()[i].classList.add("dead");
            }
        }
        elems.startGame().disabled = false;
        elems.holdGame().disabled = true;
    };

    elems.holdGame().onclick = function() {
        clearInterval(intervID);
        elems.startGame().disabled = false;
        elems.holdGame().disabled = true;
    };

    elems.randomButton().onclick = function() {
        for (var i = 0; i < elems.tds().length; i++) {
            if (elems.tds()[i].classList.contains("alive")) {
                elems.tds()[i].classList.remove("alive");
                elems.tds()[i].classList.add("dead");
            }
            if (Math.floor(Math.random() * 1.5)) {
                elems.tds()[i].classList.remove("dead");
                elems.tds()[i].classList.add("alive");
            }
        }
    }
}

function initUniverse(elems) {

    var universe = [];
    for (var i = 0; i < elems.trs().length; i++) {
        var tr = [];
        for (var j = 0; j < elems.trs()[i].cells.length; j++) {
            var pos = elems.trs()[i].cells[j].classList[1].split("_");
            var c = new Cell(pos[0], pos[1]);
            if (elems.trs()[i].cells[j].classList.contains("alive")) {
                c.alive = true;
            }
            tr.push(c);
        }
        universe.push(tr);
    }
    console.log(universe);
    return universe;

}

function initGame(elems) {

    function startGameOfLife() {

        elems.holdGame().disabled = false;
        elems.startGame().disabled = true;
        universe = initUniverse(elems);

        function nextGeneration() {
            var newUniverse = [];
            for (var i = 0; i < universe.length; i++) {
                var tr = [];
                for (var j = 0; j < universe[i].length; j++) {
                    var c = new Cell(i,j);
                    if (universe[i][j].alive) {
                        c.alive = true;
                    }
                    if (c.nextGen(universe)) {
                        c.alive = true;
                    } else {
                        c.alive = false;
                    }
                    tr.push(c);
                }
                newUniverse.push(tr);
            }

            universe = newUniverse;
            elems.nxtgen();

            for (var i = 0; i < universe.length; i++) {
                for (var j = 0; j < universe[i].length; j++) {
                    var currentCell = elems.trs()[i].cells[j];
                    currentCell.classList.remove("dead", "alive");
                    if (universe[i][j].alive) {
                        currentCell.classList.add("alive");
                    } else {
                        currentCell.classList.add("dead");
                    }
                }
            }
        }

        intervID = setInterval(nextGeneration, 100);
    }

    elems.startGame().onclick = startGameOfLife;
}

var elements = getElements();
init(elements);
initGame(elements);
